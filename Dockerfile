FROM golang:1.14.6-alpine3.12 as builder

COPY go.mod go.sum /go/src/gitlab.com/amogollonr/ms-beer/
WORKDIR /go/src/gitlab.com/amogollonr/ms-beer/
RUN go mod download
COPY . /go/src/gitlab.com/amogollonr/ms-beer
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o build/ms-beer gitlab.com/amogollonr/ms-beer


FROM alpine

RUN apk add --no-cache ca-certificates && update-ca-certificates
COPY --from=builder /go/src/gitlab.com/amogollonr/ms-beer/build/ms-beer /usr/bin/ms-beer

EXPOSE 8080 8080

ENTRYPOINT ["/usr/bin/ms-beer"]
