### Requirements
* Docker and Go
* [golang-migrate/migrate](https://github.com/golang-migrate/migrate) 

### Usage
Clone the repository with:
```bash
git clone gitlab.com/amogollonr/msbeer
```

`.env` file.
```bash
POSTGRES_USER=PG_USER
POSTGRES_PASSWORD=PG_PASS
POSTGRES_DB=PG_DATABASE
```


Update the postgres variables declared in the new `.env` to match your preference. 
There's a handy guide on the [Postgres' DockerHub](https://hub.docker.com/_/postgres).

Build and start the services with:
```bash
$ docker-compose up --build
```
The database migration files are in `db/migrations` so feel free to simply source them directly. Alternatively, you can apply them using `migrate` by running:
```bash
$ export POSTGRESQL_URL="postgres://$PG_USER:$PG_PASS@localhost:5432/$PG_DB?sslmode=disable"
$ migrate -database ${POSTGRESQL_URL} -path db/migrations up
```
_**NOTE:** Remember to replace the `$PG*` variables with their actual values_
### Development
After making your changes, you can rebuild the `server` service by running the commands below
```bash
$ docker-compose stop server
$ docker-compose build server
$ docker-compose up --no-start server
$ docker-compose start server
```

### Notas
Se agrega metodo insertar y eliminar 
 necesario para las pruebas ya que la bd estara vacia 
celular 322 730 7382
