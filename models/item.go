package models

import (
	"fmt"
	"net/http"
)

type BeerItem struct {
	Id       int     `json:"id,omitempty"`
	Name     string  `json:"name,omitempty"`
	Brewery  string  `json:"brewery,omitempty"`
	Country  string  `json:"country,omitempty"`
	Price    float32 `json:"price,omitempty"`
	Currency string  `json:"currency,omitempty"`
}

type BeerBox struct {
	PriceTotal float32 `json:"price_total,omitempty"`
}
type ItemList struct {
	Items []BeerItem `json:"items"`
}

func (i *BeerItem) Bind(r *http.Request) error {
	if i.Id < 0 {
		return fmt.Errorf("id is a required field")
	}
	return nil
}

func (*ItemList) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}

func (*BeerItem) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}
