package db

import (
	"database/sql"

	"gitlab.com/amogollonr/ms-beer/models"
)

func (db Database) GetAllBeers() (*models.ItemList, error) {
	list := &models.ItemList{}

	rows, err := db.Conn.Query("SELECT * FROM items ORDER BY ID DESC")
	if err != nil {
		return list, err
	}

	for rows.Next() {
		var item models.Item
		err := rows.Scan(&item.Id, &item.Name, &item.Brewery, &item.Country, &item.Price, &item.Currency)
		if err != nil {
			return list, err
		}
		list.Items = append(list.Items, item)
	}
	return list, nil
}

func (db Database) AddBeer(item *models.BeerItem) error {
	var id int
	var name string
	var brewery string
	var country string
	var price float32
	var currency string

	query := `INSERT INTO items (id, name, brewery, country, price, currency) VALUES ($1, $2, $3, $4, $5, $6) RETURNING id, name, brewery, country, price, currency`
	err := db.Conn.QueryRow(query, item.Id, item.Name, item.Brewery, item.Country, item.Price, item.Currency).Scan(&id, &name, &brewery, &country, &price, &currency)
	if err != nil {
		return err
	}

	item.Id = id
	item.Name = name
	item.Brewery = brewery
	item.Country = country
	item.Price = price
	item.Currency = currency
	return nil
}

func (db Database) GetBeerById(itemId int) (models.BeerItem, error) {
	item := models.BeerItem{}

	query := `SELECT * FROM items WHERE id = $1;`
	row := db.Conn.QueryRow(query, itemId)
	switch err := row.Scan(&item.Id, &item.Name, &item.Brewery, &item.Country, &item.Price, &item.Currency); err {
	case sql.ErrNoRows:
		return item, ErrNoMatch
	default:
		return item, err
	}
}

func (db Database) DeleteItem(itemId int) error {
	query := `DELETE FROM items WHERE id = $1;`
	_, err := db.Conn.Exec(query, itemId)
	switch err {
	case sql.ErrNoRows:
		return ErrNoMatch
	default:
		return err
	}
}
