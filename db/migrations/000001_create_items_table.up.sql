CREATE TABLE IF NOT EXISTS items(
id SERIAL PRIMARY KEY,
name VARCHAR(100) NOT NULL,
brewery TEXT,
country TEXT,
price decimal(10.2),
currency TEXT 
);
